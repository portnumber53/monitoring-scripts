#!/usr/bin/env php
<?php
require __DIR__ . '/common.php';

$fileSystemsToIgnore = [
  'dev', 'run', 'tmpfs', 'udev',
];

$timestamp = time();
$dfOutput = [];
$dfData = exec('df', $dfOutput);
$state = 'report';

$sendAlertEmail = false;

$pattern = '/([a-zA-Z0-9\-\/%\._]+)/';
foreach ($dfOutput as $line) {
    $fileSystem = $blocks = $used = $available = $use = $mountedOn = '';
    if (preg_match_all($pattern, $line, $matches)) {
        if (count($matches[1]) >= 6) {
            list($fileSystem, $blocks, $used, $available, $usePercent, $mountedOn) = $matches[1];

            if (is_numeric($blocks) && !in_array($fileSystem, $fileSystemsToIgnore)) {
                $diskSpaceData = [
                    'timestamp' => date('Y-m-d H:i:s', $timestamp),
                    'filesystem' => $fileSystem,
                    'size' => $blocks,
                    'used' => $used,
                    'available' => $available,
                    'mounted_on' => $mountedOn,
                ];
                
                $query = 'INSERT INTO disk_space (';
                $query .= implode(', ', array_keys($diskSpaceData));
                $query .= ') VALUES (';
                $query .= ':' . implode(',:', array_keys($diskSpaceData));
                $query .= ')';
                $stmt = $pdo->prepare($query);
                $stmt->execute($diskSpaceData);

                $log->debug('query', $diskSpaceData + ['sql' => $query]);

            }
        }
    }
}
