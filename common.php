<?php
require __DIR__ . '/vendor/autoload.php';

use Dotenv\Dotenv;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$dotenv = new Dotenv(__DIR__);
$dotenv->load();

$emailAddress = getenv('EMAIL_ADDRESS');
$emailSubject = getenv('EMAIL_SUBJECT');

$emailCcAddress = getenv('EMAIL_CC_ADDRESS');
$emailCcSubject = getenv('EMAIL_CC_NAME');

$logFilePath = getenv('LOG_FILE_PATH');
$logLevel = getenv('LOG_LEVEL');

// create a log channel
$log = new Logger('monitor');
$logLevelValue = constant("Monolog\\Logger::$logLevel");
$log->pushHandler(new StreamHandler($logFilePath, $logLevelValue));

$host = getenv('PGSQL_HOST');
$port = getenv('PGSQL_PORT');
$db = getenv('PGSQL_DATABASE');
$charset = getenv('PGSQL_CHARSET');
$user = getenv('PGSQL_USERNAME');
$pass = getenv('PGSQL_PASSWORD');

$dsn = "pgsql:host=$host;dbname=$db";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
