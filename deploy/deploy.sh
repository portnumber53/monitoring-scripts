#!/bin/bash

CURRENT_DIR=$(pwd)
USER_CI_DIR="/home/grimlock/automation/monitoring/"
SYSTEMD_DIR="/lib/systemd/system/"
REPO_URL="https://gitlab.com/portnumber53/monitoring-scripts.git"
LOG_FOLDER="/var/log/grimlock"

# Check if directory exists
if [ -d "${USER_CI_DIR}" ]
then
    echo "${USER_CI_DIR} dir present"
else
    echo "${USER_CI_DIR}, dir not present, create it"
    mkdir -p "${USER_CI_DIR}"
fi

# Check if directory is empty
if [ -z "$(ls -A ${USER_CI_DIR})" ]; then
   echo "Empty, cloning"
   git clone ${REPO_URL} ${USER_CI_DIR}
else
   echo "Not Empty, updating it"
   cd ${USER_CI_DIR}
   git checkout master
   git pull origin
   cd ..
fi

# Create log files
echo "Creating log files"
sudo mkdir -p ${LOG_FOLDER}/
sudo touch ${LOG_FOLDER}/diskspace.log ${LOG_FOLDER}/alarm.log ${LOG_FOLDER}/monitoring.log ${LOG_FOLDER}/cleanup.log
sudo chown grimlock:grimlock ${LOG_FOLDER}/diskspace.log ${LOG_FOLDER}/alarm.log ${LOG_FOLDER}/monitoring.log ${LOG_FOLDER}/cleanup.log

# Updating composer
echo "Composer update"
cd ${USER_CI_DIR}
composer install
cd ..

# Copy .env file!
# echo "Updating .env"
# cp /home/grimlock/source/monitoring-scripts/.env ${USER_CI_DIR}

# Setting up SystemD stuff
echo "Setting up SystemD stuff"
sudo cp -rvf ${USER_CI_DIR}/etc/* /etc/
sudo cp -rvf ${USER_CI_DIR}/systemd/* ${SYSTEMD_DIR}


echo "Re-loading systemd units"
sudo systemctl daemon-reload

echo "Enabling check scripts"
sudo systemctl enable check@alarm.service
sudo systemctl enable check@alarm.timer
sudo systemctl enable check@diskspace.service
sudo systemctl enable check@diskspace.timer

echo "Starting check scripts"
sudo systemctl restart check@alarm.service
sudo systemctl restart check@alarm.timer
sudo systemctl restart check@diskspace.service
sudo systemctl restart check@diskspace.timer

echo "Enabling action scripts"
sudo systemctl enable action@cleanup.service
sudo systemctl enable action@cleanup.timer

echo "Starting action scripts"
sudo systemctl restart action@cleanup.service
sudo systemctl restart action@cleanup.timer
