#!/usr/bin/env php
<?php
require __DIR__ . '/common.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$sendEmailFor = explode(',', getenv('SEND_EMAIL_FOR'));

const ALARM_SETTINGS_FIELDS = [
    'id' => '',
    'name' => '',
    'status' => '',
    'state' => '',
    'last_state' => '',
    'host' => '',
    'filesystem' => '',
    'settings' => '',
    'last_triggered' => '',
    'created_at' => '',
    'updated_at' => '',
];

// Get latest timestamps
$query = 'SELECT timestamp FROM disk_space GROUP BY timestamp ORDER BY timestamp DESC LIMIT 5';
$stmt = $pdo->prepare($query);
$stmt->execute();
$alarmTimestamps = $stmt->fetchAll();

$lastTimestamp = $alarmTimestamps[1]['timestamp'];

$alarmSettings = [];
// Get current alarm settings
$query = 'SELECT * FROM alarm_settings WHERE status=:status';
$stmt = $pdo->prepare($query);
$stmt->execute([
    'status' => 'active',
]);
$alarmSettingsData = $stmt->fetchAll();
foreach ($alarmSettingsData as $row) {
    $json = json_decode($row['settings'], true);
    $row = array_merge($row, $json);
    unset($row['settings']);
    $alarmSettings[$row['filesystem']] = $row;
}

$sendEmail = false;
$sendAlertEmail = false;
$emailMessage = '';
$emailTableDataRows = '';

$query = 'SELECT * FROM disk_space WHERE timestamp=:timestamp ORDER BY filesystem LIMIT 16';
$stmt = $pdo->prepare($query);
$stmt->execute([
    'timestamp' => $lastTimestamp,
    ]);
$diskSpaceInfo = $stmt->fetchAll();

// Let's check the state for each file system
foreach ($diskSpaceInfo as $diskSpaceInfoRow) {
    $updateAlarm = false;
    $diskSpaceInfoRow['use_percent'] = ($diskSpaceInfoRow['used'] * 100) / $diskSpaceInfoRow['size'];

    $emailTableRowTemplate = <<<TABLE
<tr style="background: __BACKGROUND_COLOR__; border-bottom: 1px solid gray">
    <td>__FILE_SYSTEM__</td>
    <td style="text-align: right">__BLOCKS__</td>
    <td style="text-align: right">__USED__</td>
    <td style="text-align: right">__AVAILABLE__</td>
    <td style="text-align: right">__USE_PERCENT__</td>
    <td>__MOUNTED_ON__</td>
    <td style="text-align: right">__THRESHOLD__ %</td>
</tr>
TABLE;

    $threshold = $alarmSettings[$diskSpaceInfoRow['filesystem']]['threshold'] ?? 100;

    $gotHealth = '';
    $use = (int) str_replace('%', '', $diskSpaceInfoRow['use_percent']);
    $backgroundColor = 'whilte';
    if ($use >= $threshold) {
        $alarmSettings[$diskSpaceInfoRow['filesystem']]['state'] = 'alarm';
        $sendAlertEmail = true;
        $backgroundColor = '#ff615b'; // red-ish
    } else {
        if (isset($alarmSettings[$diskSpaceInfoRow['filesystem']]['state'])) {
            $alarmSettings[$diskSpaceInfoRow['filesystem']]['state'] = 'idle';
        }
    }
    $emailTableDataRow = str_replace([
        '__FILE_SYSTEM__',
        '__BLOCKS__',
        '__USED__',
        '__AVAILABLE__',
        '__USE_PERCENT__',
        '__MOUNTED_ON__',
        '__THRESHOLD__',
        '__BACKGROUND_COLOR__',
    ], [
        $diskSpaceInfoRow['filesystem'],
        $diskSpaceInfoRow['size'] ,
        $diskSpaceInfoRow['used'],
        $diskSpaceInfoRow['available'],
        number_format($diskSpaceInfoRow['use_percent'], 1),
        $diskSpaceInfoRow['mounted_on'],
        $threshold,
        $backgroundColor,
    ], $emailTableRowTemplate);

    $emailTableDataRows .= $emailTableDataRow;

    $wentOff = '';
    if (isset($alarmSettings[$diskSpaceInfoRow['filesystem']])) {
        // Check alarm state
        if (($alarmSettings[$diskSpaceInfoRow['filesystem']]['last_state'] === 'idle') && ($alarmSettings[$diskSpaceInfoRow['filesystem']]['state'] === 'alarm')) {
            $wentOff = '!';
        }
        if (($alarmSettings[$diskSpaceInfoRow['filesystem']]['last_state'] === 'alarm') && ($alarmSettings[$diskSpaceInfoRow['filesystem']]['state'] === 'idle')) {
            $gotHealth = ':)';
            $updateAlarm = true;
        }

        // Check threshold
        $emailMessage .= $diskSpaceInfoRow['filesystem'] . " at $use %\n";

        $lastTriggered = $alarmSettings[$diskSpaceInfoRow['filesystem']]['last_triggered'] ?: time();
        $interval = time() - strtotime($lastTriggered);

        $sql = '';
        if ($alarmSettings[$diskSpaceInfoRow['filesystem']]['last_state'] !== $alarmSettings[$diskSpaceInfoRow['filesystem']]['state']) {
            $sql = '*';
            if ($interval > 300) {
                $sendEmail = true;
            }
            $updateAlarm = true;
        }
        if (($alarmSettings[$diskSpaceInfoRow['filesystem']]['last_state'] === $alarmSettings[$diskSpaceInfoRow['filesystem']]['state'])) {
            switch ($alarmSettings[$diskSpaceInfoRow['filesystem']]['state']) {
                case 'alarm':
                    if ($interval > 300) {
                        $sendEmail = true;
                        $updateAlarm = true;
                    }
                case 'idle':
                    if ($interval > 3600) {
                        $sendEmail = true;
                        $updateAlarm = true;
                    }
                break;
            }
        }

        if ($updateAlarm === true) {

            $newAlarmData = array_intersect_key($alarmSettings[$diskSpaceInfoRow['filesystem']], ALARM_SETTINGS_FIELDS);
            #print_r($newAlarmData);
            $settingsData = array_diff_key($alarmSettings[$diskSpaceInfoRow['filesystem']], ALARM_SETTINGS_FIELDS);
            #print_r($settingsData);
            $newAlarmData['settings'] = json_encode($settingsData);

            $newAlarmData['last_triggered'] = date('Y-m-d H:i:s', time());
            $newAlarmData['last_state'] = $newAlarmData['state'];

            $query = 'UPDATE alarm_settings SET ';
            foreach ($newAlarmData as $columnName => $columnValue) {
                if (!in_array($columnName, ['id'])) {
                    $query .= $columnName .= '=:' . $columnName . ', ';
                }
            }
            $query = substr($query, 0, -2);
            $query .= ' WHERE id=:id';
            $stmt = $pdo->prepare($query);
            $stmt->execute($newAlarmData);
        }
        $log->debug('Filesystem Info',
            $diskSpaceInfoRow + [
                'wentOff' => $wentOff,
                'gotHealth' => $gotHealth,
            ]
        );
    }
}
$seconds = date_offset_get(new DateTime);

if ($sendEmail) {
    $log->info('Sending email', []);
    $htmlBody    = file_get_contents(__DIR__ . '/template/diskspace.html');
    $htmlBody = str_replace('__DATA_ROWS__', $emailTableDataRows, $htmlBody);

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isMail();                                      // Set mailer to use SMTP

      //Recipients
        $mail->setFrom('grimlock@portnumber53.com', 'Grimlock');
        $mail->addAddress('grimlock@portnumber53.com', 'Grimlock');     // Add a recipient
        if (!empty($emailCcAddress)) {
            $mail->addCC($emailCcAddress);
        }

      //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = ($sendAlertEmail ? '[ ALARM ]' : '') . $emailSubject;
        $mail->Body    = $htmlBody;
        $mail->AltBody = 'Update your email client.';

        $mail->send();
    } catch (Exception $e) {
        $log->error('Message could not be sent.', [
          'Mailer Error' => $mail->ErrorInfo,
        ]);
    }
}
