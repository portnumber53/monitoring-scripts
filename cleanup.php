#!/usr/bin/env php
<?php
require __DIR__ . '/common.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Get latest timestamps
$query = 'SELECT timestamp FROM disk_space GROUP BY timestamp ORDER BY timestamp DESC LIMIT 5';
$stmt = $pdo->prepare($query);
$stmt->execute();
$alarmTimestamps = $stmt->fetchAll();

$lastTimestamp = $alarmTimestamps[1]['timestamp'];

$alarmSettings = [];
// Get current alarm settings
$query = 'SELECT * FROM alarm_settings WHERE status=:status AND state=:state';
$stmt = $pdo->prepare($query);
$stmt->execute([
    'status' => 'active',
    'state' => 'alarm',
]);
$alarmSettingsData = $stmt->fetchAll();

foreach ($alarmSettingsData as $row) {
    $json = json_decode($row['settings'], true);
    $row = array_merge($row, $json);
    unset($row['settings']);

    print_r($row);

    switch ($row['action']) {
        case 'remove_old':

            $basePath = $row['older_than']['from'];
            $iterator = new RecursiveDirectoryIterator($basePath);

            foreach ($iterator as $path) {
                $fileName = $path->getFileName();
                if ($fileName === '.' || $fileName === '..') {
                    continue;
                }
                $fullPath = $path->__toString();
                //$fileChanged = filemtime($fileName);

                $rmrf = '.';
                $testDate  = explode('-', $fileName);
                if (checkdate($testDate[1], $testDate[2], $testDate[0])) {
                    $creationDate = strtotime($fileName);
                    if (time() - $creationDate > $row['older_than']['amount'] * 86400) {
                        $rmrf = '!';
                    }
                }
                echo "$fullPath $rmrf\n";

                if ($rmrf === '!') {
                    $log->warning("Will remove $fullPath", []);
                    rrmdir($fullPath);
                }

              }
            break;
    }
}

function rrmdir($dir) {
    global $log;
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                $log->warning("Removing $dir/$object", []);
                if (is_dir($dir . "/" . $object)) {
                    rrmdir($dir . "/" . $object);
                } else {
                    if (! unlink($dir . "/" . $object)) {
                      $log->error("Removing $dir/$object", [
                          'info' => error_get_last(),
                      ]);
                    }
                }
            }
        }
        rmdir($dir);
    }
}
